package com.journal;

public class ThreadJoinDemo {
    public static void main(String[] args) throws InterruptedException{
        MyThread1 t = new MyThread1();
        t.start();
        t.join();
        for (int i=0;i<5;i++)
        {
            System.out.println("Main Thread");
        }
    }
}
class MyThread1 extends Thread {
    public void run() {
        for (int i = 0; i < 5; i++)
        {
            System.out.println("child thread");
            try
            {
                Thread.sleep(2000);
            }
            catch (InterruptedException e)
            {
            }
        }
    }
}