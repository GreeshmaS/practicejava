package com.journal;

public class SuSequenceEx {
    public static void main(String[] args) {
        String str = "Om Namo Namo Narayanaya";
        System.out.println(str.subSequence(str.length()-4,str.length()));
        System.out.println("First 7 " +str.subSequence(0,7));
        System.out.println("Mantra" +str.subSequence(4,11));
        System.out.println(str.substring(0,7)==str.subSequence(0,7));
        System.out.println(str.substring(0,7).equals(str.subSequence(0,7)));
        System.out.println(str.compareTo("Sari"));
        System.out.println(str.compareToIgnoreCase("sari"));

    }
}
