package com.journal;

import java.util.Arrays;

public class ByteToString {
    public static void main(String[] args) {
        String st = "WorkWorkWork";
        byte[] bytes = st.getBytes();
        System.out.println(Arrays.toString(bytes));

        //bytes to String

        byte[] b = {87, 111, 114, 107, 87, 111, 114, 107, 87, 111, 114, 107};

        String s = new String(b);
        System.out.println(s);
    }
}
